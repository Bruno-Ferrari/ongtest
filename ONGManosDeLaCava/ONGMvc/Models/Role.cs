﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ONGMvc.Models
{
    public partial class Role
    {
        public Role()
        {
            RoleUsers = new HashSet<RoleUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int UserId { get; set; }

        public virtual ICollection<RoleUser> RoleUsers { get; set; }
    }
}
