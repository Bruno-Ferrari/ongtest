﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ONGMvc.Models
{
    public partial class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Photo { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
