﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ONGMvc.Models
{
    public partial class User
    {
        public User()
        {
            RoleUsers = new HashSet<RoleUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public byte[] ProfilePhoto { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Phone { get; set; }

        public virtual ICollection<RoleUser> RoleUsers { get; set; }
    }
}
