﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ONGMvc.Models
{
    public partial class RoleUser
    {
        public int IdUser { get; set; }
        public int IdRole { get; set; }

        public virtual Role IdRoleNavigation { get; set; }
        public virtual User IdUserNavigation { get; set; }
    }
}
